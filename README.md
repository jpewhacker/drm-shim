# DRM shim - Fake GEM kernel drivers in userspace for CI

On CI systems where we don't control the kernel, it would be nice to
be able to present either no-op GEM devices (for shader-db runs) or
simulator-backed GEM devices (for testing against a software simulator
or FPGA).  This lets us do that by intercepting libc calls and
exposing render nodes.

## Limitations

- Doesn't know how to handle DRM fds getting passed over the wire from
  X11.
- libc interception is rather glibc-specific and fragile.
- Can easily break gdb if the libc interceptor code is what's broken.

## Backends

### v3d

This implements some of v3d using the closed source v3dv3 tree's
C/C++-based simulator.  All execution is synchronous.

### v3d_noop

This implements the minimum of v3d in order to make shader-db work.
The submit ioctl is stubbed out to not execute anything.

### vc4_noop

This implements the minimum of v3d in order to hopefully make
shader-db work some day.  The submit ioctl is stubbed out to not
execute anything.

## Using

You choose the backend by setting `LD_PRELOAD` to the shim you want.
Since this will effectively fake another DRM device to your system,
you may need some work on your userspace to get your test application
to use it if it's not the only DRM device present.  Setting
`DRM_SHIM_DEBUG=1` in the environment will print out what path the
shim initialized on.

For piglit tests, you can set:

```
PIGLIT_PLATFORM=gbm
WAFFLE_GBM_DEVICE=<path from DRM_SHIM_DEBUG>
```

For shader-db, use `./run -d <n>`, where n is the render node minor
number minus 128.

### v3d

Export `LD_PRELOAD=$prefix/lib/libv3d_drm_shim.so` The v3dv3 version
exposed will depend on the v3dv3 build -- 3.3, 4.1, and 4.2 are
supported.

### v3d_noop

Export `LD_PRELOAD=$prefix/lib/libv3d_noop_drm_shim.so`.  This will be
a V3D 4.2 device.

### vc4_noop

Export `LD_PRELOAD=$prefix/lib/libvc4_noop_drm_shim.so`.  This will be
the same version as in Raspberry Pi.
