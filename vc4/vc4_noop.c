/*
 * Copyright © 2018 Broadcom
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "drm-uapi/vc4_drm.h"
#include "drm_shim.h"

struct vc4_bo {
        struct shim_bo base;
};

static int
vc4_ioctl_noop(int fd, unsigned long request, void *arg)
{
        return 0;
}

static int
vc4_ioctl_create_bo(int fd, unsigned long request, void *arg)
{
        struct shim_fd *shim_fd = drm_shim_fd_lookup(fd);
        struct drm_vc4_create_bo *create = arg;
        struct vc4_bo *bo = calloc(1, sizeof(*bo));

        drm_shim_bo_init(&bo->base, create->size);

        create->handle = drm_shim_bo_get_handle(shim_fd, &bo->base);

        return 0;
}

static int
vc4_ioctl_create_shader_bo(int fd, unsigned long request, void *arg)
{
        struct shim_fd *shim_fd = drm_shim_fd_lookup(fd);
        struct drm_vc4_create_shader_bo *create = arg;
        struct vc4_bo *bo = calloc(1, sizeof(*bo));

        drm_shim_bo_init(&bo->base, create->size);

        create->handle = drm_shim_bo_get_handle(shim_fd, &bo->base);

        return 0;
}

static int
vc4_ioctl_mmap_bo(int fd, unsigned long request, void *arg)
{
        struct shim_fd *shim_fd = drm_shim_fd_lookup(fd);
        struct drm_vc4_mmap_bo *map = arg;
        struct shim_bo *bo = drm_shim_bo_lookup(shim_fd, map->handle);

        map->offset = drm_shim_bo_get_mmap_offset(shim_fd, bo);

        return 0;
}

static int
vc4_ioctl_get_tiling(int fd, unsigned long request, void *arg)
{
        struct drm_vc4_get_tiling *args = arg;

        args->modifier = 0;

        return 0;
}

static int
vc4_ioctl_get_param(int fd, unsigned long request, void *arg)
{
        struct drm_vc4_get_param *gp = arg;
        static const uint32_t getparam_map[] = {
                [DRM_VC4_PARAM_SUPPORTS_BRANCHES] = true,
                [DRM_VC4_PARAM_SUPPORTS_ETC1] = true,
                [DRM_VC4_PARAM_SUPPORTS_THREADED_FS] = true,
                [DRM_VC4_PARAM_SUPPORTS_FIXED_RCL_ORDER] = true,
                [DRM_VC4_PARAM_SUPPORTS_MADVISE] = true,
                [DRM_VC4_PARAM_SUPPORTS_PERFMON] = true,
                [DRM_VC4_PARAM_V3D_IDENT0] = 0x02000000,
                [DRM_VC4_PARAM_V3D_IDENT1] = 0x00000001,
                [DRM_VC4_PARAM_V3D_IDENT2] = 0x00000045,
        };

        if (gp->param < ARRAY_SIZE(getparam_map) && getparam_map[gp->param]) {
                gp->value = getparam_map[gp->param];
                return 0;
        }

        fprintf(stderr, "Unknown DRM_IOCTL_VC4_GET_PARAM %d\n", gp->param);
        return -1;
}

static ioctl_fn_t driver_ioctls[] = {
        [DRM_VC4_SUBMIT_CL] = vc4_ioctl_noop,
        [DRM_VC4_WAIT_BO] = vc4_ioctl_noop,
        [DRM_VC4_CREATE_BO] = vc4_ioctl_create_bo,
        [DRM_VC4_MMAP_BO] = vc4_ioctl_mmap_bo,
        [DRM_VC4_CREATE_SHADER_BO] = vc4_ioctl_create_shader_bo,
        [DRM_VC4_GET_PARAM] = vc4_ioctl_get_param,
        [DRM_VC4_GET_TILING] = vc4_ioctl_get_tiling,
};

void
drm_shim_driver_init(void)
{
        shim_device.driver_name = "vc4";
        shim_device.driver_ioctls = driver_ioctls;
        shim_device.driver_ioctl_count = ARRAY_SIZE(driver_ioctls);
}
